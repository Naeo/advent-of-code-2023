zig:
	zig run -O ReleaseFast day-01/day1.zig
	zig run -O ReleaseFast day-02/day2.zig
	zig run -O ReleaseFast day-03/day3.zig
