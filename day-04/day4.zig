const std = @import("std");
var gpa = std.heap.GeneralPurposeAllocator(.{}){};
const INPUT = @embedFile("input");
const TESTINPUT = @embedFile("testinput");

// experimenting with writing my own errors in this one
const CharNotInString = error{CharNotInString};

// I don't fully understand the logic behind how this type coercian
// is working--`*const [N:0]u8` coerced to []const u8?
// "Constant pointer to array" --> "slice of const" feels like a
// cognitive stumbling block for me so far.
fn findfirst(s: []const u8, c: u8) !usize {
    for (0..s.len) |i| {
        if (s[i] == c) {
            return i + 1;
        }
    }
    return CharNotInString.CharNotInString;
}

fn part1(input: []const u8) !u64 {
    const winner_offset: usize = try findfirst(input, ':');
    const held_offset: usize = try findfirst(input, '|');
    const linelength: usize = try findfirst(input, '\n');

    const num_winners: usize = (held_offset - winner_offset) / 3;
    const num_held: usize = (linelength - held_offset) / 3;

    var num_matches: u64 = 0;
    var total: u64 = 0;
    var lineoffset: usize = 0;
    var held: []const u8 = undefined;
    var winner: []const u8 = undefined;
    var winner_start: usize = 0;
    var held_start: usize = 0;
    const numlines = try std.math.divFloor(usize, input.len, linelength);
    // doubly nested for loop: compare each value in the 'winning' value set
    // with each value in the 'held' value set and count the number of matches.
    for (0..numlines) |line| {
        num_matches = 0;
        lineoffset = line * linelength;
        for (0..num_held) |h| {
            held_start = lineoffset + held_offset + 3 * h;
            held = input[held_start .. held_start + 3];
            for (0..num_winners) |w| {
                winner_start = lineoffset + winner_offset + 3 * w;
                winner = input[winner_start .. winner_start + 3];
                if (std.mem.eql(u8, held, winner)) {
                    num_matches += 1;
                }
            }
        }

        if (num_matches > 0) {
            total += std.math.pow(u64, 2, num_matches - 1);
        }
    }

    return total;
}

fn part2(input: []const u8) !u64 {
    // being bad and copy-pasting a lot of the part1() setup logic.
    const winner_offset: usize = try findfirst(input, ':');
    const held_offset: usize = try findfirst(input, '|');
    const linelength: usize = try findfirst(input, '\n');

    const num_winners: usize = (held_offset - winner_offset) / 3;
    const num_held: usize = (linelength - held_offset) / 3;

    var num_matches: u64 = 0;
    var lineoffset: usize = 0;
    var held: []const u8 = undefined;
    var winner: []const u8 = undefined;
    var winner_start: usize = 0;
    var held_start: usize = 0;
    const numlines = try std.math.divFloor(usize, input.len, linelength);
    const alloc = gpa.allocator();
    var scores = try alloc.alloc(u64, numlines);
    var numcards = try alloc.alloc(u64, numlines);
    defer alloc.free(scores);
    // doubly nested for loop: compare each value in the 'winning' value set
    // with each value in the 'held' value set and count the number of matches.
    for (0..numlines) |line| {
        num_matches = 0;
        lineoffset = line * linelength;
        for (0..num_held) |h| {
            held_start = lineoffset + held_offset + 3 * h;
            held = input[held_start .. held_start + 3];
            for (0..num_winners) |w| {
                winner_start = lineoffset + winner_offset + 3 * w;
                winner = input[winner_start .. winner_start + 3];
                if (std.mem.eql(u8, held, winner)) {
                    num_matches += 1;
                }
            }
        }

        scores[line] = num_matches;
        numcards[line] = 1;
    }

    // Part 2 logic: iterate through the score array and increment values
    var total: u64 = 0;
    for (0..numlines) |i| {
        for (i + 1..@min(scores.len, i + scores[i] + 1)) |j| {
            numcards[j] += numcards[i];
            //std.debug.print("Card {} gave {} copies of card {}\n", .{ i + 1, numcards[i], j + 1 });
        }
        total += numcards[i];
    }

    return total;
}

pub fn main() void {
    var start: i64 = undefined;
    var end: i64 = undefined;

    start = std.time.microTimestamp();
    const part1_res = part1(INPUT);
    end = std.time.microTimestamp();
    std.debug.print("Part 1: {!} ({}μs)\n", .{ part1_res, end - start });

    start = std.time.microTimestamp();
    const part2_res = part2(INPUT);
    end = std.time.microTimestamp();
    std.debug.print("Part 2: {!} ({}μs)\n", .{ part2_res, end - start });
}

test "part 1 test" {
    const part1_res = try part1(TESTINPUT);
    std.debug.print("Part 1 test result: {}\n", .{part1_res});
    try std.testing.expect(part1_res == 13);
}

test "part 2 test" {
    const part2_res = try part2(TESTINPUT);
    std.debug.print("Part 2 test result: {}\n", .{part2_res});
    try std.testing.expect(part2_res == 30);
}
