const std = @import("std");
var gpa = std.heap.GeneralPurposeAllocator(.{}){};

const INPUT = @embedFile("input");
const TESTINPUT = @embedFile("testinput");

const Part = struct {
    start: i64,
    end: i64,
    value: u64,
};

fn overlaps(part: Part, symbol_position: usize, linelength: usize) bool {
    const posn: i64 = @bitCast(symbol_position);
    const len: i64 = @bitCast(linelength);
    const ul = posn - len - 1;
    const uc = posn - len;
    const ur = posn - len + 1;
    const left = posn - 1;
    const right = posn + 1;
    const ll = posn + len - 1;
    const lc = posn + len;
    const lr = posn + len + 1;

    if ((ul >= part.start and ul <= part.end) or (ur >= part.start and ur <= part.end) or (uc >= part.start and uc <= part.end) or left == part.end or right == part.start or (ll >= part.start and ll <= part.end) or (lr >= part.start and lr <= part.end) or (lc >= part.start and lc <= part.end)) {
        return true;
    } else {
        return false;
    }
}

fn part1(comptime L: usize, input: *const [L:0]u8) !u64 {
    //get line length dynamically
    var linelength: usize = 0;
    while (true) {
        linelength += 1;
        if (input[linelength] == '\n') {
            break;
        }
    }
    // account for newline characters
    linelength += 1;

    // get all of the part numbers
    const allocator = gpa.allocator();
    var parts = std.ArrayList(Part).init(allocator);
    var symbols = std.ArrayList(usize).init(allocator);
    defer parts.deinit();
    defer symbols.deinit();

    var i: usize = 0;
    var part_number_len: usize = 0;
    while (i < input.len) {
        if (input[i] >= '0' and input[i] <= '9') {
            // parse out the digits
            for (i..input.len) |j| {
                if (input[j] < '0' or input[j] > '9') {
                    part_number_len = j - i;
                    break;
                }
            }

            try parts.append(Part{
                .start = @bitCast(i),
                .end = @bitCast(i + part_number_len - 1),
                .value = try std.fmt.parseInt(u64, input[i .. i + part_number_len], 10),
            });
            i += part_number_len - 1;
        } else if (input[i] != '.' and input[i] != '\n') {
            try symbols.append(i);
        }
        i += 1;
    }

    // avoid double-counting
    var hash = std.AutoHashMap(Part, void).init(allocator);
    var total: u64 = 0;
    for (symbols.items) |s| {
        for (parts.items) |p| {
            if (overlaps(p, s, linelength)) {
                try hash.put(p, {});
            }
        }
    }

    var vals = hash.keyIterator();
    while (vals.next()) |p| {
        total += p.value;
    }
    return total;
}

fn part2(comptime L: usize, input: *const [L:0]u8) !u64 {
    //get line length dynamically
    var linelength: usize = 0;
    while (true) {
        linelength += 1;
        if (input[linelength] == '\n') {
            break;
        }
    }
    // account for newline characters
    linelength += 1;

    // get all of the part numbers
    const allocator = gpa.allocator();
    var parts = std.ArrayList(Part).init(allocator);
    var symbols = std.ArrayList(usize).init(allocator);
    defer parts.deinit();
    defer symbols.deinit();

    var i: usize = 0;
    var part_number_len: usize = 0;
    while (i < input.len) {
        if (input[i] >= '0' and input[i] <= '9') {
            // parse out the digits
            for (i..input.len) |j| {
                if (input[j] < '0' or input[j] > '9') {
                    part_number_len = j - i;
                    break;
                }
            }

            try parts.append(Part{
                .start = @bitCast(i),
                .end = @bitCast(i + part_number_len - 1),
                .value = try std.fmt.parseInt(u64, input[i .. i + part_number_len], 10),
            });
            i += part_number_len - 1;
        } else if (input[i] == '*') {
            try symbols.append(i);
        }
        i += 1;
    }

    // avoid double-counting
    var total: u64 = 0;
    var n_gears: u64 = 0;
    var tmp_total: u64 = 1;
    for (symbols.items) |s| {
        n_gears = 0;
        tmp_total = 1;
        for (parts.items) |p| {
            if (overlaps(p, s, linelength)) {
                tmp_total *= p.value;
                n_gears += 1;
            }
        }
        if (n_gears == 2) {
            total += tmp_total;
        }
    }

    return total;
}

pub fn main() void {
    var start = std.time.microTimestamp();
    const part_1_res = part1(INPUT.len, INPUT) catch unreachable;
    var end = std.time.microTimestamp();
    std.debug.print("Part 1: {any} ({}us)\n", .{ part_1_res, end - start });

    start = std.time.microTimestamp();
    const part_2_res = part2(INPUT.len, INPUT);
    end = std.time.microTimestamp();
    std.debug.print("Part 2: {any} ({}us)\n", .{ part_2_res, end - start });
}

test "part 1 test" {
    const part1_test_val = try part1(TESTINPUT.len, TESTINPUT);
    std.debug.print("Test value for part 1: {}\n", .{part1_test_val});
    try std.testing.expect(part1_test_val == 4361);
}

test "part 2 test" {
    const part2_test_val = try part2(TESTINPUT.len, TESTINPUT);
    std.debug.print("Test value for part 2: {}\n", .{part2_test_val});
    try std.testing.expect(part2_test_val == 467835);
}
