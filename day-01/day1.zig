const std = @import("std");
const INPUT = @embedFile("input");
const TESTINPUT =
    \\1abc2
    \\pqr3stu8vwx
    \\a1b2c3d4e5f
    \\treb7uchet
;

fn part1() u64 {
    // solve part 1 by iteratively parsing raw bytes
    var total: u64 = 0;
    var digit1: u64 = 0;
    var digit2: u64 = 0;
    var curdigit: u64 = 0;

    for (INPUT) |char| {
        // 48-57 is the ASCII digit range
        if (char >= '0' and char <= '9') {
            curdigit = char - '0';
        } else if (char == '\n' or char == 0) {
            total += 10 * digit1 + digit2;
            curdigit = 0;
            digit1 = 0;
            digit2 = 0;
            continue;
        }

        if (digit1 == 0) {
            digit1 = curdigit;
        }
        digit2 = curdigit;
    }

    // above 'else' case doesn't catch the final exit from the string
    // since the iteration stops before emittin the termination/sentinel byte
    total += 10 * digit1 + digit2;

    return total;
}

fn part2() u64 {
    // solve part 2 similarly to part 1, but on start letters
    // for digit names, check if the rest of the line == the
    // name of the digit, too.
    var total: u64 = 0;
    var digit1: u64 = 0;
    var digit2: u64 = 0;
    var curdigit: u64 = 0;
    var char: u8 = ' ';
    const digits = [_][]const u8{
        "one",
        "two",
        "three",
        "four",
        "five",
        "six",
        "seven",
        "eight",
        "nine",
    };

    for (0..INPUT.len) |idx| {
        char = INPUT[idx];
        if (char >= '0' and char <= '9') {
            curdigit = char - '0';
        } else if (char == '\n') {
            total += 10 * digit1 + digit2;
            curdigit = 0;
            digit1 = 0;
            digit2 = 0;
            continue;
        } else {
            for (digits, 1..) |str, i| {
                if (INPUT.len >= idx + str.len and std.mem.startsWith(u8, INPUT[idx..], str)) {
                    curdigit = i;
                    break;
                }
            }
        }

        if (digit1 == 0) {
            digit1 = curdigit;
        }
        digit2 = curdigit;
    }
    total += 10 * digit1 + digit2;
    return total;
}

pub fn main() void {
    var start = std.time.microTimestamp();
    const part1_res = part1();
    var end = std.time.microTimestamp();
    std.debug.print("Part 1: {} ({}us)\n", .{ part1_res, end - start });

    start = std.time.microTimestamp();
    const part2_res = part2();
    end = std.time.microTimestamp();
    std.debug.print("Part 2: {} ({}us)\n", .{ part2_res, end - start });
}
