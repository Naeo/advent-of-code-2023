# Advent of Code 2023

My Advent of Code 2023 solutions.  This year I'm focusing on learning new languages, primarily Go
and Zig, but I might do early days' solutions in some other funky languages.  Expect some Haskell,
Julia, maybe LISP, maybe Forth, and so on.  Really just whatever I feel like.  I'm also using this
to get more comfortable with git, so expect the repo to be a bit of a disaster if you look to close.

Each day's folder contains code and a makefile to build + run everything in it as needed.

Requirements:
- zig 0.12 (or compatible)
- go 1.21.4 (or compatible)

All code is written assuming a 64-bit Linux system.  So your mileage may vary on other operating
systems.
