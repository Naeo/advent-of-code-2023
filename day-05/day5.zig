const std = @import("std");
const print = std.debug.print;
const ArrayList = std.ArrayList;
var gpa = std.heap.GeneralPurposeAllocator(.{}){};
const INPUT = @embedFile("input");
const TESTINPUT = @embedFile("testinput");

const SeedMap = struct {
    src_lower: i64,
    src_upper: i64,
    // offset is how much to add to values in the current range
    offset: i64,
};

const ParseErrors = error{
    CharNotFound,
};

fn findfirst(text: []const u8, target: u8) !usize {
    for (0..text.len) |i| {
        if (text[i] == target) {
            return i;
        }
    }
    return ParseErrors.CharNotFound;
}

fn parse_seeds(input: []const u8) ![]i64 {
    const allocator = gpa.allocator();
    var seeds = std.ArrayList(i64).init(allocator);

    const firstline = findfirst(input, '\n') catch {
        print("Could not find newline in the input.\n", .{});
        std.os.exit(1);
    };
    //consume the first chunk--the "seeds:" line
    var seed_iter = std.mem.tokenizeScalar(u8, input[0..firstline], ' ');
    //skip "seeds:" token
    _ = seed_iter.next();
    while (seed_iter.next()) |s| {
        var parsed: i64 = try std.fmt.parseInt(i64, s, 10);
        try seeds.append(parsed);
    }
    return seeds.items;
}

fn parse_seeds_part2(input: []const u8) ![]i64 {
    const allocator = gpa.allocator();
    var seeds = std.ArrayList(i64).init(allocator);

    const firstline = findfirst(input, '\n') catch {
        print("Could not find newline in the input.\n", .{});
        std.os.exit(1);
    };
    //consume the first chunk--the "seeds:" line
    var seed_iter = std.mem.tokenizeScalar(u8, input[0..firstline], ' ');
    //skip "seeds:" token
    _ = seed_iter.next();
    while (true) {
        var start_str = seed_iter.next();
        var len_str = seed_iter.next();
        if (start_str == null or len_str == null) {
            break;
        }
        var start = try std.fmt.parseInt(i64, start_str.?, 10);
        var len = try std.fmt.parseInt(i64, len_str.?, 10);
        var i: i64 = 0;
        while (i < len) {
            try seeds.append(start + i);
            i += 1;
        }
    }
    return seeds.items;
}

// parse the input into the different chunks.
// This function creates memory that other functions own--namely part1()
// and part2()--and must free themselves.
fn parse_maps(input: []const u8) ![][]SeedMap {
    const allocator = gpa.allocator();
    var maps = ArrayList([]SeedMap).init(allocator);
    // parse out the rest of the maps; each chunk is separated by \n\n
    var chunks = std.mem.tokenizeSequence(u8, input, "\n\n");

    // parse out the rest of the maps
    _ = chunks.next(); //skip the "seeds:" line
    var curmapname: []const u8 = "";
    while (chunks.next()) |chunk| {
        var tokens = std.mem.tokenizeAny(u8, chunk, ":\n ");
        var curmap = std.ArrayList(SeedMap).init(allocator);
        curmapname = tokens.next().?;
        // consume the literal "map" token
        _ = tokens.next();
        while (true) {
            var dst_str = tokens.next();
            var src_str = tokens.next();
            var len_str = tokens.next();
            if (dst_str == null or src_str == null or len_str == null) {
                break;
            }
            var src = try std.fmt.parseInt(i64, src_str.?, 10);
            var dst = try std.fmt.parseInt(i64, dst_str.?, 10);
            var len = try std.fmt.parseInt(i64, len_str.?, 10);
            try curmap.append(SeedMap{ .src_lower = src, .src_upper = src + len - 1, .offset = dst - src });
        }
        try maps.append(curmap.items);
    }

    return maps.items;
}

fn part1(input: []const u8) i64 {
    var seeds = parse_seeds(input) catch |err| {
        print("Error when parsing seeds: {!}\n", .{err});
        std.os.exit(1);
    };
    var seedmaps = parse_maps(input) catch |err| {
        print("Error in part 1: {!}\n", .{err});
        std.os.exit(1);
    };

    for (seedmaps) |curmaps| {
        for (0..seeds.len) |j| {
            for (curmaps) |curmap| {
                if (seeds[j] >= curmap.src_lower and seeds[j] <= curmap.src_upper) {
                    seeds[j] += curmap.offset;
                    break;
                }
            }
        }
    }

    var minlocation: i64 = seeds[0];
    for (seeds) |s| {
        if (s < minlocation) {
            minlocation = s;
        }
    }

    return minlocation;
}

fn part2(input: []const u8) i64 {
    // this is killing my memory.  So many numbers!!
    var seeds = parse_seeds_part2(input) catch |err| {
        print("Error when parsing seeds: {!}\n", .{err});
        std.os.exit(1);
    };
    var seedmaps = parse_maps(input) catch |err| {
        print("Error in part 1: {!}\n", .{err});
        std.os.exit(1);
    };

    for (seedmaps) |curmaps| {
        for (0..seeds.len) |j| {
            for (curmaps) |curmap| {
                if (seeds[j] >= curmap.src_lower and seeds[j] <= curmap.src_upper) {
                    seeds[j] += curmap.offset;
                    break;
                }
            }
        }
    }

    var minlocation: i64 = seeds[0];
    for (seeds) |s| {
        if (s < minlocation) {
            minlocation = s;
        }
    }

    return minlocation;
}

pub fn main() void {
    var start: i64 = undefined;
    var end: i64 = undefined;

    start = std.time.microTimestamp();
    const part1_res = part1(INPUT);
    end = std.time.microTimestamp();
    std.debug.print("Part 1: {} ({}μs)\n", .{ part1_res, end - start });

    start = std.time.microTimestamp();
    const part2_res = part2(INPUT);
    end = std.time.microTimestamp();
    std.debug.print("Part 2: {} ({}μs)\n", .{ part2_res, end - start });
}

test "part 1 test" {
    const part1_res = part1(TESTINPUT);
    std.debug.print("Part 1 test result: {}\n", .{part1_res});
    try std.testing.expect(part1_res == 35);
}

test "part 2 test" {
    const part2_res = part2(TESTINPUT);
    std.debug.print("Part 2 test result: {}\n", .{part2_res});
    try std.testing.expect(part2_res == 46);
}
