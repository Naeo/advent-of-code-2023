const std = @import("std");
const INPUT = @embedFile("input");
const TESTINPUT =
    \\Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
    \\Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
    \\Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
    \\Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
    \\Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
;

fn game_number(record: []const u8) u64 {
    if (record.len < 5) {
        return 0;
    }
    // var gamenum: u64 = 0;
    var end: usize = 5; // first digit in every record is at position 5
    while (true) {
        if (record[end + 1] == ':' or end > record.len) {
            break;
        }
        end = end + 1;
    }

    // this does not feel like the right way to handle error unions?
    return std.fmt.parseInt(u64, record[5 .. end + 1], 10) catch unreachable;
}

fn is_possible(record: []const u8) bool {
    // extract: "D+ c" where D is a digit and C is 'r', 'g', or 'b'.
    var amount: u64 = 0;
    var color: u8 = ' ';
    var idx: usize = 0;

    // seek to the location of the ':' that marks the start of the record
    while (true) {
        if (record[idx] == ':') {
            break;
        }
        idx += 1;
    }

    // iterate and find amounts by color
    var tokens = std.mem.tokenizeScalar(u8, record, ' ');
    // consume the 'Game X:' tokens
    _ = tokens.next();
    _ = tokens.next();

    var tok1: ?[]const u8 = undefined;
    var tok2: ?[]const u8 = undefined;
    while (true) {
        tok1 = tokens.next();
        tok2 = tokens.next();
        // null --> end of tokens
        if (tok1 == null or tok2 == null) {
            break;
        }

        amount = std.fmt.parseInt(u64, tok1.?, 10) catch unreachable;
        color = tok2.?[0];

        if ((color == 'b' and amount > 14) or (color == 'r' and amount > 12) or (color == 'g' and amount > 13)) {
            return false;
        }
    }

    return true;
}

fn max(a: u64, b: u64) u64 {
    if (a > b) {
        return a;
    } else {
        return b;
    }
}

fn power(record: []const u8) u64 {
    // extract: "D+ c" where D is a digit and C is 'r', 'g', or 'b'.
    var amount: u64 = 0;
    var color: u8 = ' ';
    var blue: u64 = 0;
    var green: u64 = 0;
    var red: u64 = 0;
    var idx: usize = 0;

    // seek to the location of the ':' that marks the start of the record
    while (true) {
        if (record[idx] == ':') {
            break;
        }
        idx += 1;
    }
    //
    // iterate and find amounts by color
    var tokens = std.mem.tokenizeScalar(u8, record, ' ');
    // consume the 'Game X:' tokens
    _ = tokens.next();
    _ = tokens.next();

    var tok1: ?[]const u8 = undefined;
    var tok2: ?[]const u8 = undefined;
    while (true) {
        tok1 = tokens.next();
        tok2 = tokens.next();
        // null --> end of tokens
        if (tok1 == null or tok2 == null) {
            break;
        }

        amount = std.fmt.parseInt(u64, tok1.?, 10) catch unreachable;
        color = tok2.?[0];

        switch (color) {
            'r' => {
                red = max(red, amount);
            },
            'g' => {
                green = max(green, amount);
            },
            'b' => {
                blue = max(blue, amount);
            },
            else => {},
        }
    }

    return red * blue * green;
}

fn part1(comptime L: usize, games: *const [L:0]u8) u64 {
    var total: u64 = 0;

    var lines = std.mem.splitScalar(u8, games, '\n');
    while (lines.next()) |line| {
        if (line.len == 0) {
            break;
        }
        if (is_possible(line)) {
            total += game_number(line);
        }
    }

    return total;
}

fn part2(comptime L: usize, games: *const [L:0]u8) u64 {
    var total: u64 = 0;

    var lines = std.mem.splitScalar(u8, games, '\n');
    while (lines.next()) |line| {
        if (line.len == 0) {
            break;
        }
        total += power(line);
    }

    return total;
}

pub fn main() void {
    var start = std.time.microTimestamp();
    const part1_res = part1(INPUT.len, INPUT);
    var end = std.time.microTimestamp();
    std.debug.print("Part 1: {} ({}us)\n", .{ part1_res, end - start });

    start = std.time.microTimestamp();
    const part2_res = part2(INPUT.len, INPUT);
    end = std.time.microTimestamp();
    std.debug.print("Part 2: {} ({}us)\n", .{ part2_res, end - start });
}

test "part 1 test" {
    try std.testing.expect(part1(TESTINPUT.len, TESTINPUT) == 8);
}
test "part 2 test" {
    try std.testing.expect(part2(TESTINPUT.len, TESTINPUT) == 2286);
}
